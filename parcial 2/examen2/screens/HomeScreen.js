import React from 'react';
import { View, Text, StyleSheet, ScrollView, Image } from 'react-native';

// Importa las imágenes
const image1 = require('../assets/image.jpg');
const gifImage = require('../assets/nfl.jpeg');

// Colores
const azulOscuro = '#02151E';

const HomeScreen = () => {
    return (
        <ScrollView style={styles.container}>
            <View style={styles.section}>
                <View style={styles.textContainer}>
                    <Text style={styles.heading}>Bienvenido a la NFL</Text>
                    <Text style={styles.paragraph}></Text>
                </View>
                <View style={styles.imageContainer}>
                    <Image source={image1} style={styles.image} />
                </View>
            </View>

            <View style={styles.section}>
                <View style={styles.textContainer}>
                    <Text style={styles.heading}>¿Cuál es el propósito de la NFL aplication?</Text>
                    <Text style={styles.paragraph}>
                    El propósito de la aplicación es proporcionar una interfaz de usuario para acceder y administrar una lista de tareas pendientes (todos) de la liga de la NFL. La aplicación permite a los usuarios ver diferentes aspectos de la lista de tareas, como todos los pendientes, pendientes sin resolver, pendientes resueltos, entre otros. 
                        </Text>
                </View>
                <View style={styles.imageContainer}>
                    <Image source={gifImage} style={styles.gifImage} />
                </View>
            </View>

        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1c1c1e',
    },
    section: {
        paddingHorizontal: 20,
        paddingBottom: 30,
        marginBottom: 20,
    },
    textContainer: {
        marginBottom: 20,
    },
    heading: {
        color: '#fff',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
        textAlign: 'center', // Centrar el texto
    },
    paragraph: {
        color: '#fff',
        fontSize: 16,
        textAlign: 'center', // Centrar el texto
    },
    imageContainer: {
        alignItems: 'center',
        marginBottom: 20,
    },
    image: {
        width: 200,
        height: 200, // Ajusta la altura de la imagen
        borderRadius: 40,
    },
    gifImage: {
        width: 350,
        height: 200,
        borderRadius: 10,
    },
    courseContainer: {
        flexWrap: 'wrap', // Añade envoltura
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 10, // Agrega margen superior
    },
    course: {
        backgroundColor: azulOscuro,
        borderRadius: 10,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
        width: '48%', // Ajusta el ancho del curso
        marginBottom: 20, // Añade margen inferior para separación
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    courseImage: {        width: 100,
        height: 100, // Ajusta la altura de la imagen
        marginBottom: 10,
        borderRadius: 10,
    },
    courseTitle: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center', // Centrar el texto
        marginBottom: 10,
    },
    courseDuration: {
        color: '#fff',
        fontSize: 14,
        textAlign: 'center', // Centrar el texto
    },
});

export default HomeScreen;