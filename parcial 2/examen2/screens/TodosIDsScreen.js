import React, { useState, useEffect } from 'react';
import { View, ScrollView } from 'react-native';
import { DataTable } from 'react-native-paper';
import { fetchTodos, mapTodosToIDs } from './api';

export default function TodosIDsScreen() {
  const [todosIDs, setTodosIDs] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const todos = await fetchTodos();
        const ids = mapTodosToIDs(todos);
        setTodosIDs(ids);
      } catch (error) {
        console.error('Error fetching todos IDs:', error);
      }
    }

    fetchData();
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={{ backgroundColor: '#f0f0f0', padding: 20 }}>
          <DataTable style={{ borderWidth: 1, borderColor: '#ddd' }}>
            <DataTable.Header>
              <DataTable.Title>ID</DataTable.Title>
            </DataTable.Header>
  
            {todosIDs.map(id => (
              <DataTable.Row key={id}>
                <DataTable.Cell>{id}</DataTable.Cell>
              </DataTable.Row>
            ))}
          </DataTable>
        </View>
      </ScrollView>
    </View>
  );
}
