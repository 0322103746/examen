const fetch = require('node-fetch');

async function obtenerPendientes() {
    const respuesta = await fetch('http://jsonplaceholder.typicode.com/todos');
    return await respuesta.json();
}

async function mostrarMenu() {
    console.log('Seleccione una opción:');
    console.log('1. Lista de todos los pendientes (sólo IDs)');
    console.log('2. Lista de todos los pendientes (IDs y Títulos)');
    console.log('3. Pendientes Sin Resolver (ID y Título)');
    console.log('4. Pendientes Resueltos (ID y Título)');
    console.log('5. Lista de todos los pendientes (IDs y UserIDs)');
    console.log('6. Pendientes Resueltos (ID y UserID)');
    console.log('7. Pendientes Sin Resolver (ID y UserID)');
    console.log('0. Salir');

    const opcion = await obtenerOpcionUsuario();

    if (opcion !== 0) {
        switch (opcion) {
            case 1:
                await listarIDs();
                break;
            case 2:
                await listarIDsyTitulos();
                break;
            case 3:
                await listarPendientesSinResolver();
                break;
            case 4:
                await listarPendientesResueltos();
                break;
            case 5:
                await listarIDsyUserIDs();
                break;
            case 6:
                await listarPendientesResueltosConUserIDs();
                break;
            case 7:
                await listarPendientesSinResolverConUserIDs();
                break;
            default:
                console.log('Opción no válida. Intente nuevamente.');
                break;
        }

        await mostrarMenu();
    } else {
        console.log('Saliendo de la aplicación.');
    }
}

async function obtenerOpcionUsuario() {
    return new Promise((resolver) => {
        const readline = require('readline');
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.question('Ingrese el número de la opción deseada: ', (respuesta) => {
            rl.close();
            resolver(parseInt(respuesta));
        });
    });
}

async function listarIDs() {
    const pendientes = await obtenerPendientes();
    const IDs = pendientes.map(pendiente => pendiente.id);
    
    console.log('Lista de todos los pendientes (sólo IDs):');
    console.log(IDs);
}

async function listarIDsyTitulos() {
    const pendientes = await obtenerPendientes();
    const IDsyTitulos = pendientes.map(pendiente => ({ id: pendiente.id, title: pendiente.title }));
    
    console.log('Lista de todos los pendientes (IDs y Títulos):');
    console.log(IDsyTitulos);
}

async function listarPendientesSinResolver() {
    const pendientes = await obtenerPendientes();
    const sinResolver = pendientes.filter(pendiente => !pendiente.completed).map(pendiente => ({ id: pendiente.id, title: pendiente.title }));
    
    console.log('Pendientes Sin Resolver (ID y Título):');
    console.log(sinResolver);
}

async function listarPendientesResueltos() {
    const pendientes = await obtenerPendientes();
    const resueltos = pendientes.filter(pendiente => pendiente.completed).map(pendiente => ({ id: pendiente.id, title: pendiente.title }));
    
    console.log('Pendientes Resueltos (ID y Título):');
    console.log(resueltos);
}

async function listarIDsyUserIDs() {
    const pendientes = await obtenerPendientes();
    const IDsyUserIDs = pendientes.map(pendiente => ({ id: pendiente.id, userId: pendiente.userId }));
    
    console.log('Lista de todos los pendientes (IDs y UserIDs):');
    console.log(IDsyUserIDs);
}

async function listarPendientesResueltosConUserIDs() {
    const pendientes = await obtenerPendientes();
    const resueltosYUserIDs = pendientes.filter(pendiente => pendiente.completed).map(pendiente => ({ id: pendiente.id, userId: pendiente.userId }));
    
    console.log('Pendientes Resueltos (ID y UserID):');
    console.log(resueltosYUserIDs);
}

async function listarPendientesSinResolverConUserIDs() {
    const pendientes = await obtenerPendientes();
    const sinResolverYUserIDs = pendientes.filter(pendiente => !pendiente.completed).map(pendiente => ({ id: pendiente.id, userId: pendiente.userId }));
    
    console.log('Pendientes Sin Resolver (ID y UserID):');
    console.log(sinResolverYUserIDs);
}

async function main() {
    await mostrarMenu();
}

main();
